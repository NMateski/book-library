package com.creativehub.booklibrary.city;

import com.creativehub.booklibrary.domain.city.*;
import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {

    @InjectMocks
    private CityService cityService;

    @Mock
    private CityRepository cityRepository;

    @Test
    public void given_create_city_request__when_create_city__then_created_city() {
        when(cityRepository.save(any(City.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, City.class));

        var request = new CityRequest("name");
        var expectedResult = new CityDTO(null, request.name);
        var result = cityService.create(request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_valid_city_id__when_update_city__then_update_city() {
        when(cityRepository.findById(any()))
                .thenReturn(Optional.of(new City()));
        when(cityRepository.save(any(City.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, City.class));

        var request = new CityRequest("name");
        var expectedResult = new CityDTO(null, request.name);
        var result = cityService.update(0, request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_city_id__when_update_city__then_throw_exception() {
        when(cityRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> cityService.findById(0));
    }

    @Test
    public void given_valid_city_id__when_delete_city__then_delete_city() {
        var city = new City();
        when(cityRepository.findById(any()))
                .thenReturn(Optional.of(city));

        cityService.delete(0);

        verify(cityRepository, times(1)).delete(city);
    }

    @Test
    public void given_invalid_city_id__when_delete_city__then_throw_exception() {
        when(cityRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> cityService.delete(0));
    }

    @Test
    public void given_valid_city_id__when_find_by_id__then_return_city() {
        when(cityRepository.findById(any()))
                .thenReturn(Optional.of(new City()));

        var expectedResult = new CityDTO();
        var result = cityService.findById(0);

        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_city_id__when_find_by_id__then_throw_exception() {
        when(cityRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> cityService.findById(0));
    }
}
