package com.creativehub.booklibrary.category;

import com.creativehub.booklibrary.domain.category.*;
import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @InjectMocks
    private CategoryService categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @Test
    public void given_create_category_request__when_create_category__then_created_category() {
        when(categoryRepository.save(any(Category.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Category.class));

        var request = new CategoryRequest("name");
        var expectedResult = new CategoryDTO(null, request.name);
        var result = categoryService.create(request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_valid_category_id__when_update_category__then_update_category() {
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.of(new Category()));
        when(categoryRepository.save(any(Category.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Category.class));

        var request = new CategoryRequest("name");
        var expectedResult = new CategoryDTO(null, request.name);
        var result = categoryService.update(0, request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_category_id__when_update_category__then_throw_exception() {
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> categoryService.update(0, new CategoryRequest()));
    }

    @Test
    public void given_valid_category_id__when_delete_category__then_delete_category() {
        var category = new Category();
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.of(category));

        categoryService.delete(0);

        verify(categoryRepository, times(1)).delete(category);
    }

    @Test
    public void given_invalid_category_id__when_delete_category__then_throw_exception() {
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> categoryService.delete(0));
    }

    @Test
    public void given_valid_category_id__when_find_by_id__then_return_category() {
        var category = new Category();
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.of(new Category()));

        var result = categoryService.findById(0);
        var expectedResult = category.toDTO();

        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_category_id__when_find_by_id__then_throw_exception() {
        when(categoryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> categoryService.findById(0));
    }
}
