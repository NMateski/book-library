package com.creativehub.booklibrary.user;

import com.creativehub.booklibrary.domain.city.City;
import com.creativehub.booklibrary.domain.city.CityRepository;
import com.creativehub.booklibrary.domain.country.Country;
import com.creativehub.booklibrary.domain.country.CountryRepository;
import com.creativehub.booklibrary.domain.user.User;
import com.creativehub.booklibrary.domain.user.UserRepository;
import com.creativehub.booklibrary.domain.user.UserRequest;
import com.creativehub.booklibrary.domain.user.UserService;
import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private CityRepository cityRepository;

    @Test
    public void given_create_user_request__when_create_user__then_created_user() {
        when(countryRepository.findById(any())).thenReturn(Optional.of(new Country()));
        when(cityRepository.findById(any())).thenReturn(Optional.of(new City()));
        when(userRepository.save(any(User.class))).thenAnswer(answer -> answer.getArgument(0, User.class));

        var request = new UserRequest("firstName", "lastName", Instant.now(), "address",
                1000, "email@email.com", "username", 123L, "password", true);
        var expectedUser = request.toEntity();
        expectedUser.city = new City();
        expectedUser.country = new Country();
        var expectedUserDTO = expectedUser.toDTO();
        var userDTO = userService.create(0, 0, request);

        assertNotNull(userDTO);
        assertEquals(expectedUserDTO, userDTO);
    }

    @Test
    public void given_invalid_country_id__when_create_user__then_throw_exception() {
        when(countryRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.create(0, 0, new UserRequest()));
    }

    @Test
    public void given_invalid_city_id__when_create_user__then_throw_exception() {
        when(countryRepository.findById(any())).thenReturn(Optional.of(new Country()));
        when(cityRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.create(0, 0, new UserRequest()));
    }

    @Test
    public void given_update_user_request__when_update_user__then_updated_user() {
        when(userRepository.findById(any())).thenReturn(Optional.of(new User()));
        when(userRepository.save(any(User.class))).thenAnswer(answer -> answer.getArgument(0, User.class));

        var request = new UserRequest("firstName", "lastName", Instant.now(), "address",
                1000, "email@email.com", "username", 123L, "password", true);
        var expectedUserDTO = request.toEntity().toDTO();
        var userDTO = userService.update(0L, request);

        assertNotNull(userDTO);
        assertEquals(expectedUserDTO, userDTO);
    }

    @Test
    public void given_invalid_user_id__when_update_user__then_throw_exception() {
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.update(0L, new UserRequest()));
    }

    @Test
    public void given_valid_user_id__when_delete_user__then_user_deleted() {
        when(userRepository.findById(any())).thenReturn(Optional.of(new User()));

        userService.delete(0L);

        verify(userRepository, times(1)).delete(any());
    }

    @Test
    public void given_invalid_user_id__when_delete_user__then_throw_exception() {
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.delete(0L));
    }

    @Test
    public void given_valid_user_id__when_find_by_id__then_user_found() {
        var user = new User();
        when(userRepository.findById(any())).thenReturn(Optional.of(user));

        var expectedUserDTO = user.toDTO();
        var userDTO = userService.findById(0L);

        assertNotNull(userDTO);
        assertEquals(expectedUserDTO, userDTO);
    }

    @Test
    public void given_invalid_user_id__when_find_by_id__then_throw_exception() {
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.findById(0L));
    }
}
