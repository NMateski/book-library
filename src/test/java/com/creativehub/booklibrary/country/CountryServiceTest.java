package com.creativehub.booklibrary.country;

import com.creativehub.booklibrary.domain.country.*;
import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {

    @InjectMocks
    private CountryService countryService;

    @Mock
    private CountryRepository countryRepository;

    @Test
    public void given_create_country_request__when_create_country__then_created_country() {
        when(countryRepository.save(any(Country.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Country.class));

        var request = new CountryRequest("name");
        var expectedResult = new CountryDTO(null, request.name);
        var result = countryService.create(request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_valid_country_id__when_update_country__then_update_country() {
        when(countryRepository.findById(any()))
                .thenReturn(Optional.of(new Country()));
        when(countryRepository.save(any(Country.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Country.class));

        var request = new CountryRequest("name");
        var expectedResult = new CountryDTO(null, request.name);
        var result = countryService.update(0, request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_country_id__when_update_country__then_throw_exception() {
        when(countryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> countryService.update(0, new CountryRequest()));
    }

    @Test
    public void given_valid_country_id__when_delete_country__then_delete_country() {
        var country = new Country();
        when(countryRepository.findById(any()))
                .thenReturn(Optional.of(country));

        countryService.delete(0);

        verify(countryRepository, times(1)).delete(country);
    }

    @Test
    public void given_invalid_country_id__when_delete_country__then_throw_exception() {
        when(countryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> countryService.delete(0));
    }

    @Test
    public void given_valid_country_id__when_find_by_id__then_return_country() {
        var country = new Country();
        when(countryRepository.findById(any()))
                .thenReturn(Optional.of(new Country()));

        var result = countryService.findById(0);
        var expectedResult = country.toDTO();

        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_country_id__when_find_by_id__then_throw_exception() {
        when(countryRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> countryService.findById(0));
    }
}
