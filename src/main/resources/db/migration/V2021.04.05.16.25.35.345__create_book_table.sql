CREATE TABLE categories
(
    id BIGSERIAL NOT NULL,
    name character varying(150) NOT NULL,
    CONSTRAINT category_pkey PRIMARY KEY(id)
);

CREATE TABLE country
(
    id BIGSERIAL NOT NULL,
    name character varying(150) NOT NULL,
    CONSTRAINT country_pkey PRIMARY KEY(id)
);

CREATE TABLE city
(
    id BIGSERIAL NOT NULL,
    name character varying(150) NOT NULL,
    CONSTRAINT city_pkey PRIMARY KEY(id)
);

CREATE TABLE users
(
    id BIGSERIAL NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    address character varying(300) NOT NULL,
    city_id BIGSERIAL NOT NULL,
    country_id BIGSERIAL NOT NULL,
    zip INTEGER NOT NULL,
    email character varying(100),
    username character varying(150) NOT NULL,
    phone BIGINT NOT NULL,
    password character varying(300) NOT NULL,
    should_receive_promo_email BOOLEAN,
    CONSTRAINT user_pkey PRIMARY KEY(id),
    CONSTRAINT FK_city_user FOREIGN KEY (city_id) REFERENCES city (id),
    CONSTRAINT FK_country_user FOREIGN KEY (country_id) REFERENCES country (id)

);

CREATE TABLE author
(
    id BIGSERIAL NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    date_of_birth timestamp with time zone NOT NULL,
    country_id BIGINT NOT NULL,
    CONSTRAINT author_pkey PRIMARY KEY(id),
    CONSTRAINT FK_country_author FOREIGN KEY (country_id) REFERENCES country (id)
);

CREATE TABLE books
(
    id BIGSERIAL NOT NULL,
    title character varying(250) NOT NULL,
    author_id BIGINT NOT NULL,
    date_of_publish timestamp with time zone NOT NULL,
    isbn character varying(50) NOT NULL,
    price numeric(8, 2) NOT NULL,
    version BIGINT,
    CONSTRAINT book_pkey PRIMARY KEY (id),
    CONSTRAINT FK_author_books FOREIGN KEY (author_id) REFERENCES author (id)
);