package com.creativehub.booklibrary;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
  This is an MVC controller
 */
@AllArgsConstructor
@Controller
public class SpaController {

    @GetMapping({"/{path:[^\\.]*}", "/**/{path:^(?!oauth).*}/{path:[^\\.]*}"})
    public String forward(HttpServletRequest request) {
        return "forward:/index.html";
    }
}
