package com.creativehub.booklibrary.domain.category;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CategoryRequest {

    public String name;

    public Category toEntity() {
        return new Category(name);
    }
}
