package com.creativehub.booklibrary.domain.category;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class CategorySearchRequest {

    public String name;
    public Pageable pageable;

    public CategorySearchRequest(String name, Pageable pageable) {
        this.name = name;
        this.pageable = pageable;
    }

    public Specification<Category> generateSpecification() {
        Specification<Category> categorySpecification = Specification.where(null);

        if (StringUtils.hasText(name)) {
            categorySpecification = categorySpecification.and(CategorySpecifications.byNameLiteralEquals(name));
        }

        return categorySpecification;
    }
}
