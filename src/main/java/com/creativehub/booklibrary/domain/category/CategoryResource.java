package com.creativehub.booklibrary.domain.category;

import com.creativehub.booklibrary.util.VoidDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categories")
public class CategoryResource {

    private CategoryService categoryService;

    public CategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CategoryDTO create(@RequestBody final CategoryRequest categoryRequest) {
        return categoryService.create(categoryRequest);
    }

    @PutMapping(path = "/{categoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CategoryDTO update(@PathVariable("categoryId") final Integer categoryId,
                              @RequestBody final CategoryRequest categoryRequest) {
        return categoryService.update(categoryId, categoryRequest);
    }

    @DeleteMapping(path = "/{categoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("categoryId") final Integer categoryId) {
        categoryService.delete(categoryId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<CategoryDTO> findPage(
            @RequestParam(value = "name", required = false) final String name, Pageable pageable) {
        return categoryService.findPage(new CategorySearchRequest(name, pageable));
    }

    @GetMapping(path = "/{categoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CategoryDTO findById(@PathVariable("categoryId") final Integer categoryId) {
        return categoryService.findById(categoryId);
    }
}
