package com.creativehub.booklibrary.domain.category;

import org.springframework.data.jpa.domain.Specification;

public final class CategorySpecifications {

    public CategorySpecifications() {
    }

    public static Specification<Category> byNameLiteralEquals(final String name) {
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")), name.toLowerCase()));
    }
}
