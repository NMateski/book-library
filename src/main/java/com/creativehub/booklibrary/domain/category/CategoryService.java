package com.creativehub.booklibrary.domain.category;

import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public CategoryDTO create(final CategoryRequest categoryRequest) {
        var category = categoryRequest.toEntity();
        return categoryRepository.save(category).toDTO();
    }

    public CategoryDTO update(final Integer categoryId, final CategoryRequest categoryRequest) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category with id:" + categoryId + " not found!"));

        category.name = categoryRequest.name;
        return categoryRepository.save(category).toDTO();
    }

    public void delete(final Integer categoryId) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category with id:" + categoryId + " not found!"));
        categoryRepository.delete(category);
    }

    public List<CategoryDTO> findAll() {
        return categoryRepository.findAll().stream()
                .map(category -> category.toDTO())
                .collect(Collectors.toList());
    }

    public CategoryDTO findById(final Integer categoryId) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category with id:" + categoryId + " not found!"));

        return category.toDTO();
    }

    public Page<CategoryDTO> findPage(final CategorySearchRequest request) {
        return categoryRepository.findAll(request.generateSpecification(), request.pageable)
                .map(Category::toDTO);
    }
}
