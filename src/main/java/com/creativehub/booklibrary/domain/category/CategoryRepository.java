package com.creativehub.booklibrary.domain.category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<Category, Integer>, JpaSpecificationExecutor<Category> {

    @Query(nativeQuery = true,
            value = "SELECT * FROM categories " +
                    " WHERE lower(name) = COALESCE(lower(:name), lower(name)) ",
            countQuery = "SELECT count(id) FROM inventory " +
                    " WHERE lower(name) = COALESCE(lower(:name), lower(name)) ")
    Page<Category> findPage(final String name, final Pageable pageable);
}
