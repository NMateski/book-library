package com.creativehub.booklibrary.domain.user;

import com.creativehub.booklibrary.util.VoidDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserResource {

    private final UserService userService;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public UserDTO create(@Valid @RequestBody final UserRequest userRequest,
                          @RequestParam("countryId") final Integer countryId,
                          @RequestParam("cityId") final Integer cityId) {
        return userService.create(countryId, cityId, userRequest);
    }

    @PutMapping(path = "/{userId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public UserDTO update(@Valid @PathVariable("userId") final Long userId,
                          @RequestBody final UserRequest userRequest) {
        return userService.update(userId, userRequest);
    }

    @DeleteMapping(path = "/{userId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("userId") final Long userId) {
        userService.delete(userId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

    @GetMapping(path = "/{userId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public UserDTO findById(@PathVariable("userId") final Long userId) {
        return userService.findById(userId);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<UserDTO> findPage(
            @RequestParam(value = "searchPhrase", required = false) final String searchPhrase,
            @RequestParam(value = "cityId", required = false) final Long cityId,
            @RequestParam(value = "countryId", required = false) final Long countryId,
            @PageableDefault(sort = "username", direction = Sort.Direction.DESC) Pageable pageable) {
        return userService.findPage(new UserSearchRequest(searchPhrase, cityId, countryId, pageable));
    }
}
