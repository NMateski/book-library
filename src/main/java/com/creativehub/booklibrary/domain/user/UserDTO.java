package com.creativehub.booklibrary.domain.user;

import com.creativehub.booklibrary.domain.city.CityDTO;
import com.creativehub.booklibrary.domain.country.CountryDTO;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class UserDTO {

    public Long id;
    public String firstName;
    public String lastName;
    public Instant dateOfBirth;
    public String address;
    public CityDTO city;
    public CountryDTO country;
    public Integer zip;
    public String email;
    public String username;
    public Long phone;
    public String password;
    public Boolean shouldReceivePromoEmail;
}
