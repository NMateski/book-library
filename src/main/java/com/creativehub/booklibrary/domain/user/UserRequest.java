package com.creativehub.booklibrary.domain.user;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    @NotNull
    @NotBlank
    public String firstName;

    @NotNull
    @NotBlank
    public String lastName;

    @NotNull
    public Instant dateOfBirth;

    @NotNull
    @NotBlank
    public String address;

    @NotNull
    public Integer zip;

    @NotNull
    @NotBlank
    @Email
    public String email;

    @NotNull
    @NotBlank
    public String username;

    @NotNull
    public Long phone;

    @NotNull
    @NotBlank
    public String password;


    public Boolean shouldReceivePromoEmail;

    public User toEntity() {
        return new User(firstName, lastName, dateOfBirth, address, zip, email, username, phone, password,
                shouldReceivePromoEmail);
    }


}
