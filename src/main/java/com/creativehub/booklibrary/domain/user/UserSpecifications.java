package com.creativehub.booklibrary.domain.user;

import org.springframework.data.jpa.domain.Specification;

public class UserSpecifications {

    public UserSpecifications() {
    }

    public static Specification<User> bySearchPhrase(final String searchPhrase) {
        var searchPhraseToLower = "%" + searchPhrase.toLowerCase() + "%";
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.or(
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("lastName")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("address")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("email")), searchPhraseToLower),
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), searchPhraseToLower)
                );
    }

    public static Specification<User> byCityId(final Long cityId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("city").get("id"), cityId);
    }

    public static Specification<User> byCountryId(final Long countryId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("country").get("id"), countryId);
    }
}
