package com.creativehub.booklibrary.domain.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class UserSearchRequest {

    public String searchPhrase;
    public Long cityId;
    public Long countryId;
    public Pageable pageable;

    public UserSearchRequest(String searchPhrase, Long cityId, Long countryId, Pageable pageable) {
        this.searchPhrase = searchPhrase;
        this.cityId = cityId;
        this.countryId = countryId;
        this.pageable = pageable;
    }

    public Specification<User> generateSpecification() {
        Specification<User> userSpecification = Specification.where(null);

        if (StringUtils.hasText(searchPhrase)) {
            userSpecification = userSpecification.and(UserSpecifications.bySearchPhrase(searchPhrase));
        }

        if (cityId != null) {
            userSpecification = userSpecification.and(UserSpecifications.byCityId(cityId));
        }

        if (countryId != null) {
            userSpecification = userSpecification.and(UserSpecifications.byCountryId(countryId));
        }

        return userSpecification;
    }
}
