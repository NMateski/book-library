package com.creativehub.booklibrary.domain.user;

import com.creativehub.booklibrary.domain.city.City;
import com.creativehub.booklibrary.domain.country.Country;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "users")
@ToString
public class User {

    @Id
    @GeneratedValue
    public Long id;

    public String firstName;
    public String lastName;
    public Instant dateOfBirth;
    public String address;
    public Integer zip;
    public String email;
    public String username;
    public Long phone;
    public String password;
    public Boolean shouldReceivePromoEmail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    @ToString.Exclude
    public City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @ToString.Exclude
    public Country country;

    public User() {
    }

    public User(String firstName, String lastName, Instant dateOfBirth, String address, Integer zip,
                String email, String username, Long phone, String password, Boolean shouldReceivePromoEmail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.zip = zip;
        this.email = email;
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.shouldReceivePromoEmail = shouldReceivePromoEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User entity = (User) o;
        return id.equals(entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }

    public UserDTO toDTO() {
        var cityDTO = city != null ? city.toDTO() : null;
        var countryDTO = country != null ? country.toDTO() : null;
        return new UserDTO(id, firstName, lastName, dateOfBirth, address, cityDTO, countryDTO, zip, email, username, phone, password,
                shouldReceivePromoEmail);
    }
}
