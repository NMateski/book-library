package com.creativehub.booklibrary.domain.user;

import com.creativehub.booklibrary.domain.city.CityRepository;
import com.creativehub.booklibrary.domain.country.CountryRepository;
import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;

    public UserDTO create(final Integer countryId, final Integer cityId, final UserRequest userRequest) {
        var country = countryRepository.findById(countryId)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id: " + countryId + " not found!"));
        var city = cityRepository.findById(cityId)
                .orElseThrow(() -> new ResourceNotFoundException("City with id: " + cityId + " not found!"));

        var user = userRequest.toEntity();
        user.country = country;
        user.city = city;

        return userRepository.save(user).toDTO();
    }

    public UserDTO update(final Long userId, final UserRequest userRequest) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id: " + userId + " not found!"));

        user.firstName = userRequest.firstName;
        user.lastName = userRequest.lastName;
        user.dateOfBirth = userRequest.dateOfBirth;
        user.address = userRequest.address;
        user.zip = userRequest.zip;
        user.email = userRequest.email;
        user.username = userRequest.username;
        user.phone = userRequest.phone;
        user.password = userRequest.password;
        user.shouldReceivePromoEmail = userRequest.shouldReceivePromoEmail;

        //TODO add changes for updating city and country for user (also change test)

        return userRepository.save(user).toDTO();
    }

    public void delete(final Long userId) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id:" + userId + " not found!"));
        userRepository.delete(user);
    }

    public UserDTO findById(final Long userId) {
        var user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id:" + userId + " not found!"));

        return user.toDTO();
    }

    public Page<UserDTO> findPage(final UserSearchRequest request) {
        return userRepository.findAll(request.generateSpecification(), request.pageable)
                .map(User::toDTO);
    }
}
