package com.creativehub.booklibrary.domain.city;

import org.springframework.data.jpa.domain.Specification;

public class CitySpecifications {

    public CitySpecifications() {
    }

    public static Specification<City> byNameLiteralEquals(final String name) {
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")), name.toLowerCase()));
    }
}
