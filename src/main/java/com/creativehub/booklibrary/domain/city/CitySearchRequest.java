package com.creativehub.booklibrary.domain.city;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class CitySearchRequest {

    public String name;
    public Pageable pageable;

    public CitySearchRequest(String name, Pageable pageable) {
        this.name = name;
        this.pageable = pageable;
    }

    public Specification<City> generateSpecification() {
        Specification<City> citySpecification = Specification.where(null);

        if (StringUtils.hasText(name)) {
            citySpecification = citySpecification.and(CitySpecifications.byNameLiteralEquals(name));
        }

        return citySpecification;
    }
}
