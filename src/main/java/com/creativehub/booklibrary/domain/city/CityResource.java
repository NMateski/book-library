package com.creativehub.booklibrary.domain.city;

import com.creativehub.booklibrary.util.VoidDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/city")
@RequiredArgsConstructor
public class CityResource {

    private final CityService cityService;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CityDTO create(@RequestBody final CityRequest cityRequest) {
        return cityService.create(cityRequest);
    }

    @PutMapping(path = "/{cityId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CityDTO update(@PathVariable("cityId") final Integer cityId,
                          @RequestBody final CityRequest cityRequest) {
        return cityService.update(cityId, cityRequest);
    }

    @DeleteMapping(path = "/{cityId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("cityId") final Integer cityId) {
        cityService.delete(cityId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<CityDTO> findPage(
            @RequestParam(value = "name", required = false) final String name, Pageable pageable) {
        return cityService.findPage(new CitySearchRequest(name, pageable));
    }

    @GetMapping(path = "/{cityId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CityDTO findById(@PathVariable final Integer cityId) {
        return cityService.findById(cityId);
    }
}
