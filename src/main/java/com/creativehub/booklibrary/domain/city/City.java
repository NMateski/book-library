package com.creativehub.booklibrary.domain.city;

import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "city")
@ToString
public class City {

    @Id
    @GeneratedValue
    public Integer id;

    public String name;

    public City() {
    }

    public City(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City entity = (City) o;
        return id.equals(entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }

    public CityDTO toDTO() {
        return new CityDTO(id, name);
    }
}
