package com.creativehub.booklibrary.domain.city;

import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CityService {

    private final CityRepository cityRepository;

    public CityDTO create(final CityRequest cityRequest){
        var city = cityRequest.toEntity();
        return cityRepository.save(city).toDTO();
    }

    public CityDTO update(final Integer cityId, final CityRequest cityRequest) {
        var city = cityRepository.findById(cityId)
                .orElseThrow(() -> new ResourceNotFoundException("City with id: " + cityId + " not found!"));

        city.name = cityRequest.name;
        return cityRepository.save(city).toDTO();
    }

    public void delete(final Integer cityId) {
        var city = cityRepository.findById(cityId)
                .orElseThrow(() -> new ResourceNotFoundException("City with id: " + cityId + " not found!"));

        cityRepository.delete(city);
    }

    public Page<CityDTO> findPage(final CitySearchRequest request) {
        return cityRepository.findAll(request.generateSpecification(), request.pageable)
                .map(City::toDTO);
    }

    public CityDTO findById(final Integer cityId) {
        var city = cityRepository.findById(cityId)
                .orElseThrow(() -> new ResourceNotFoundException("City with id: " + cityId + " not found!"));

        return city.toDTO();
    }
}
