package com.creativehub.booklibrary.domain.city;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CityRequest {

    public String name;

    public City toEntity() {
        return new City(name);
    }
}
