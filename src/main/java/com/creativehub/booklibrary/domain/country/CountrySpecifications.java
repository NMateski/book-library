package com.creativehub.booklibrary.domain.country;

import org.springframework.data.jpa.domain.Specification;

public class CountrySpecifications {

    public CountrySpecifications() {
    }

    public static Specification<Country> byNameLiteralEquals(final String name) {
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")), name.toLowerCase()));
    }
}
