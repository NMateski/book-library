package com.creativehub.booklibrary.domain.country;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class CountrySearchRequest {

    public String name;
    public Pageable pageable;

    public CountrySearchRequest(String name, Pageable pageable) {
        this.name = name;
        this.pageable = pageable;
    }

    public Specification<Country> generateSpecification() {
        Specification<Country> countrySpecification = Specification.where(null);

        if (StringUtils.hasText(name)) {
            countrySpecification = countrySpecification.and(CountrySpecifications.byNameLiteralEquals(name));
        }

        return countrySpecification;
    }
}
