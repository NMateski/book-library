package com.creativehub.booklibrary.domain.country;

import com.creativehub.booklibrary.util.VoidDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/countries")
@RequiredArgsConstructor
public class CountryResource {

    public final CountryService countryService;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CountryDTO create(@RequestBody final CountryRequest countryRequest) {
        return countryService.create(countryRequest);
    }

    @PutMapping(path = "/{countryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CountryDTO update(@PathVariable("countryId") final Integer countryId,
                             @RequestBody final CountryRequest countryRequest) {
        return countryService.update(countryId, countryRequest);
    }

    @DeleteMapping(path = "/{countryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("countryId") final Integer countryId) {
        countryService.delete(countryId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

    @GetMapping(path = "/{countryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public CountryDTO findById(@PathVariable("countryId") final Integer countryId) {
        return countryService.findById(countryId);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<CountryDTO> findPage(
            @RequestParam(value = "name", required = false) final String name, Pageable pageable) {
        return countryService.findPage(new CountrySearchRequest(name, pageable));
    }
}
