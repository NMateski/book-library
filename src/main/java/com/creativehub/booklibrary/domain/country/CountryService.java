package com.creativehub.booklibrary.domain.country;

import com.creativehub.booklibrary.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountryService {

    private final CountryRepository countryRepository;

    public CountryDTO create(final CountryRequest countryRequest) {
        var country = countryRequest.toEntity();
        return countryRepository.save(country).toDTO();
    }

    public CountryDTO update(final Integer countryId, final CountryRequest countryRequest) {
        var country = countryRepository.findById(countryId)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id: " + countryId + " not found!"));

        country.name = countryRequest.name;
        return countryRepository.save(country).toDTO();
    }

    public void delete(final Integer countryId) {
        var country = countryRepository.findById(countryId)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id:" + countryId + " not found!"));
        countryRepository.delete(country);
    }

    public CountryDTO findById(final Integer countryId) {
        var country = countryRepository.findById(countryId)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id:" + countryId + " not found!"));

        return country.toDTO();
    }

    public Page<CountryDTO> findPage(final CountrySearchRequest request) {
        return countryRepository.findAll(request.generateSpecification(), request.pageable)
                .map(Country::toDTO);
    }
}
