package com.creativehub.booklibrary.domain.country;

import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "country")
@ToString
public class Country {

    @Id
    @GeneratedValue
    public Integer id;

    public String name;

    public Country() {
    }

    public Country(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country entity = (Country) o;
        return id.equals(entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }

    public CountryDTO toDTO() {
        return new CountryDTO(id, name);
    }
}
