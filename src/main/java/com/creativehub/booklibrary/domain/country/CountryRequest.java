package com.creativehub.booklibrary.domain.country;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CountryRequest {

    public String name;

    public Country toEntity() {
        return new Country(name);
    }
}
