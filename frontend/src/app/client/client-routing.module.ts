import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './book/book.component';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {path: '', component: ClientComponent, children: [
    {path: 'books', children: [
      {path: '', component: BookComponent},
    ]},
    {path: '**', redirectTo: 'books'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
