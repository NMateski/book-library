import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client',
  template: '<app-authenticated-layout></app-authenticated-layout>'
})
export class ClientComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
