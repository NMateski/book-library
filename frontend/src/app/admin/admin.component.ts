import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template: '<app-authenticated-layout></app-authenticated-layout>'
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
