import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(private router: Router) {
    this.formGroup = new FormGroup({
      userName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(250)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.maxLength(100)
      ])
    });
   }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    console.log('Loging in!');
    this.router.navigateByUrl('clients');
  }

}
